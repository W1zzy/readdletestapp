//
//  MailCoordinator.swift
//  MailClient
//
//  Created by Антон Братчик on 12.07.2021.
//

import UIKit

class MailCoordinator: Coordinating {

    // MARK: Injected
    
    private let splitView: UISplitViewController
    var mailService: (MailSource & MailListSource)!
    var notificationService: NotificationServicing?

    // MARK: Lifecycle

    required init(splitView: UISplitViewController) {
        self.splitView = splitView
    }

    // MARK: Coordinating
    
    func start() {
        let mailList: MailListViewController = MailListViewController.instantiate(with: .mailList)
        let mailListPresenter = MailListPresenter(mailSource: mailService)
        mailListPresenter.view = mailList
        mailListPresenter.navigationDelegate = self
        mailList.presenter = mailListPresenter

        let mail: MailViewController = MailViewController.instantiate(with: .mail)

        splitView.delegate = self
        splitView.preferredDisplayMode = .oneBesideSecondary
        splitView.viewControllers = [UINavigationController(rootViewController: mailList),
                                     UINavigationController(rootViewController: mail)]

        notificationService?.delegate = self
        notificationService?.checkAuthorization()
    }

}

// MARK: - MailListNavigationDelegate

extension MailCoordinator: MailListNavigationDelegate {

    func openMail(_ mail: Mail) {
        let mailViewController: MailViewController = MailViewController.instantiate(with: .mail)
        let mailPresenter = MailPresenter(id: mail.id,
                                          mailSource: mailService)
        mailPresenter.view = mailViewController
        mailPresenter.navigationDelegate = self
        mailViewController.presenter = mailPresenter

        splitView.showDetailViewController(UINavigationController(rootViewController: mailViewController),
                                           sender: nil)
    }

}

// MARK: - MailListNavigationDelegate

extension MailCoordinator: MailNavigationDelegate {

    func closeMail() {
        if let navController = splitView.viewControllers[0] as? UINavigationController {
            navController.popViewController(animated: true)
        }
    }

}

// MARK: - NotificationServiceDelegate

extension MailCoordinator: NotificationServiceDelegate {

    func noPermissionsGranted() {
        let alert = UIAlertController(title: nil,
                                      message: "We need to send you notifications, go to settings?",
                                      preferredStyle: .alert)
        let yes = UIAlertAction(title: "Settings", style: .default) { _ in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }

            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl)
            }
        }
        let no = UIAlertAction(title: "Not now", style: .cancel)

        alert.addAction(yes)
        alert.addAction(no)

        splitView.present(alert, animated: true)
    }

}

// MARK: - UISplitViewControllerDelegate

extension MailCoordinator: UISplitViewControllerDelegate {

    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        return true
    }
}

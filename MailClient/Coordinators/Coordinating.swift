//
//  Coordinating.swift
//  MailClient
//
//  Created by Антон Братчик on 12.07.2021.
//

import UIKit

protocol Coordinating: AnyObject {

    init(splitView: UISplitViewController)
    func start()
    
}

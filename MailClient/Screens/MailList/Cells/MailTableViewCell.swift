//
//  MailTableViewCell.swift
//  MailClient
//
//  Created by Антон Братчик on 07.07.2021.
//

import UIKit

protocol MailTableViewCellDelegate: AnyObject {

    func mailCellChangeReadStatus(_ cell: MailTableViewCell)
    func mailCellDeleteItem(_ cell: MailTableViewCell)

}

class MailTableViewCell: UITableViewCell {

    // MARK: Injected

    weak var delegate: MailTableViewCellDelegate?

    // MARK: Outlets
    
    @IBOutlet private weak var readIndicatorView: UIView!
    @IBOutlet private weak var fromLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var subjectLabel: UILabel!
    @IBOutlet private weak var previewLabel: UILabel!

    // MARK: Properties

    private var isRead: Bool = false {
        didSet {
            readIndicatorView.isHidden = isRead
        }
    }

    // MARK: Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
        let interaction = UIContextMenuInteraction(delegate: self)
        self.addInteraction(interaction)
    }
}

// MARK: - UIContextMenuInteractionDelegate

extension MailTableViewCell: UIContextMenuInteractionDelegate {

    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
        let configuration = UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { [weak self] actions -> UIMenu? in
            guard let self = self else { return nil }
            let readUnread = UIAction(title: self.isRead ? "Unread": "Read") { action in
                self.delegate?.mailCellChangeReadStatus(self)
            }

            let delete = UIAction(title: "Delete") { action in
                self.delegate?.mailCellDeleteItem(self)
            }

            return UIMenu(title: "Actions", children: [readUnread, delete])
        }
        return configuration
    }

}

// MARK: - ModelTransfer

extension MailTableViewCell: ModelTransfer {

    func update(with model: MailListViewModel) {
        isRead = model.isRead
        fromLabel.text = model.from
        subjectLabel.text = model.subject
        dateLabel.text = model.date.formattedString
        previewLabel.text = model.preview.components(separatedBy: .newlines).joined(separator: String.space)
    }

}

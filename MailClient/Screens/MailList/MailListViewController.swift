//
//  MailListViewController.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import UIKit

protocol MailListInput: AnyObject, AlertShowing, ActivityIndicatorHolding {

    func showEmptyView(_ show: Bool)

    func reloadData()
    func inserOneOnTop()
    func inserItems(count: Int)
    func reloadItem(at index: Int)
    func deleteItem(at index: Int)
    func blockInfinityScroll(_ block: Bool)
    
}

class MailListViewController: UIViewController {

    // MARK: Injected

    var presenter: MailListPresenter?

    // MARK: Outlets

    @IBOutlet private weak var emptyLabel: UILabel!
    @IBOutlet private weak var mailTableView: UITableView! {
        didSet {
            MailTableViewCell.registerFor(tableView: mailTableView)
            
            mailTableView.delegate = self
            mailTableView.dataSource = self
        }
    }
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    // MARK: Properties

    var infinityScrollBlocked: Bool = false
    
    // MARK: Lifecycle

    deinit {
        presenter?.viewUnloaded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Inbox"
        presenter?.viewLoaded()
    }

}

// MARK: - MailListInput

extension MailListViewController: MailListInput {

    func showEmptyView(_ show: Bool) {
        mailTableView.isHidden = show
        emptyLabel.isHidden = !show
    }

    func reloadData() {
        mailTableView.reloadData()
    }

    func inserOneOnTop() {
        mailTableView.insertRows(at: [IndexPath(row: .zero, section: .zero)], with: .fade)
    }

    func inserItems(count: Int) {
        let lastRowIndex = mailTableView.numberOfRows(inSection: .zero) - 1
        var indexPaths = [IndexPath]()

        (1...count).forEach {
            indexPaths.append(IndexPath(row: lastRowIndex + $0, section: .zero))
        }

        mailTableView.insertRows(at: indexPaths, with: .fade)
    }

    func reloadItem(at index: Int) {
        mailTableView.reloadRows(at: [IndexPath(row: index, section: .zero)],
                                 with: .none)
    }

    func deleteItem(at index: Int) {
        mailTableView.deleteRows(at: [IndexPath(row: index, section: .zero)],
                                 with: .fade)
    }

    func blockInfinityScroll(_ block: Bool) {
        infinityScrollBlocked = block
    }

}

// MARK: - PaginatableView

extension MailListViewController: PaginatableView {

    func scrollViewDidScrollOverThreshold() {
        presenter?.loadNextPage()
    }

}

// MARK: - MailTableViewCellDelegate

extension MailListViewController: MailTableViewCellDelegate {

    func mailCellChangeReadStatus(_ cell: MailTableViewCell) {
        guard let index = mailTableView.indexPath(for: cell) else { return }
        presenter?.changeReadStatus(for: index.row)
    }

    func mailCellDeleteItem(_ cell: MailTableViewCell) {
        guard let index = mailTableView.indexPath(for: cell) else { return }
        presenter?.delete(index: index.row)
    }

}

// MARK: - UITableViewDelegate

extension MailListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.mailSelected(at: indexPath.row)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollViewScrolled(scrollView)
    }

}

// MARK: - UITableViewDataSource

extension MailListViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.countOfItems() ?? .zero
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let model = presenter?.itemFor(index: indexPath.row) else { return UITableViewCell() }

        let cell: MailTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.delegate = self
        cell.update(with: model)
        return cell
    }

}

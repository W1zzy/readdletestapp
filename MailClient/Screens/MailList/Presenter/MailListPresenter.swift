//
//  MailListPresenter.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import Foundation

protocol MailListNavigationDelegate: AnyObject {

    func openMail(_ mail: Mail)

}

protocol MailListPresenting: Presenting {

    func loadNextPage()

    func countOfItems() -> Int
    func itemFor(index: Int) -> MailListViewModel?

    func mailSelected(at index: Int)
    func changeReadStatus(for index: Int)
    func delete(index: Int)

}

class MailListPresenter {

    // MARK: Injected

    private var mailSource: MailListSource

    weak var view: MailListInput?
    weak var navigationDelegate: MailListNavigationDelegate?

    // MARK: Properties

    private var loaded: Bool = false

    init(mailSource: MailListSource) {
        self.mailSource = mailSource
    }

}

// MARK: - MailListPresenting

extension MailListPresenter: MailListPresenting {

    func viewLoaded() {
        mailSource.addListener(self)
        view?.startLoading()
        view?.blockInfinityScroll(true)
        mailSource.fetchMailList()
    }

    func loadNextPage() {
        view?.blockInfinityScroll(true)
        mailSource.fetchMailList()
    }

    func viewUnloaded() {
        mailSource.removeListener(self)
    }

    func countOfItems() -> Int {
        let count = mailSource.mailsCount()
        view?.showEmptyView(count == .zero && loaded)
        return count
    }

    func itemFor(index: Int) -> MailListViewModel? {
        MailListViewModel(with: mailSource.itemFor(index: index))
    }

    func mailSelected(at index: Int) {
        navigationDelegate?.openMail(mailSource.itemSelectedFor(index: index))
    }

    func changeReadStatus(for index: Int) {
        mailSource.changeReadStatus(at: index)
    }

    func delete(index: Int) {
        mailSource.delete(at: index)
    }

}

// MARK: - MailServiceListener

extension MailListPresenter: MailServiceListener {

    func reload(at index: Int) {
        view?.reloadItem(at: index)
    }

    func deleted(at index: Int) {
        view?.deleteItem(at: index)
    }

    func update() {
        loaded.toggle()
        view?.stopLoading()
        view?.reloadData()
        view?.blockInfinityScroll(false)
    }

    func appendOneAtTop() {
        view?.inserOneOnTop()
    }

    func append(count: Int) {
        view?.inserItems(count: count)
        view?.blockInfinityScroll(false)
    }

    func received(error: String) {
        view?.stopLoading()
        view?.showError(error)
    }

    func lastPageReceived() {
        view?.blockInfinityScroll(true)
    }

}

// MARK: - MailListViewModel mapper

private extension MailListViewModel {

    init(with mail: Mail) {
        date = mail.date
        from = mail.from
        subject = mail.subject
        preview = mail.preview
        isRead = mail.isRead
    }

}

//
//  MailListViewModel.swift
//  MailClient
//
//  Created by Антон Братчик on 07.07.2021.
//

import Foundation

struct MailListViewModel {
    let date: Date
    let from: String
    let subject: String
    let preview: String
    let isRead: Bool
}

//
//  MailViewModel.swift
//  MailClient
//
//  Created by Антон Братчик on 12.07.2021.
//

import Foundation

struct MailViewModel {
    let date: Date
    let from: String
    let subject: String
}

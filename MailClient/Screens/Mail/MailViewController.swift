//
//  MailViewController.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import UIKit

protocol MailInput: AnyObject, AlertShowing, ActivityIndicatorHolding {

    func showEmptyView()

    func updateView(with viewModel: MailViewModel)
    func updateView(with content: String?)
    func updateReadStatus(_ status: Bool)

}

class MailViewController: UIViewController {

    // MARK: Injected

    var presenter: MailPresenting?

    @IBOutlet private weak var fromLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var contentLabel: UILabel!
    @IBOutlet private weak var emptyLabel: UILabel!
    @IBOutlet private weak var unreadButton: UIBarButtonItem!
    @IBOutlet private weak var deleteButton: UIBarButtonItem! {
        didSet {
            deleteButton.title = "Delete"
        }
    }
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private var isRead: Bool = false {
        didSet {
            unreadButton.title = isRead ? "Unread" : "Read"
        }
    }

    // MARK: Lifecycle

    deinit {
        presenter?.viewUnloaded()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if let presenter = presenter {
            presenter.viewLoaded()
        } else {
            showEmptyView()
        }
    }

    // MARK: - Actions

    @IBAction func unreadTapped(_ sender: UIBarButtonItem) {
        presenter?.changeReadStatus()
    }

    @IBAction func deleteTapped(_ sender: UIBarButtonItem) {
        presenter?.delete()
    }

}

// MARK: - MailInput

extension MailViewController: MailInput {

    func showEmptyView() {
        navigationItem.title = nil

        emptyLabel.isHidden = false

        fromLabel.isHidden = true
        contentLabel.isHidden = true
        dateLabel.isHidden = true
        deleteButton.title = nil
        unreadButton.title = nil
    }

    func updateView(with viewModel: MailViewModel) {
        navigationItem.title = viewModel.subject
        fromLabel.text = "From \(viewModel.from)"
        dateLabel.text = viewModel.date.formattedString
    }

    func updateView(with content: String?) {
        contentLabel.text = content
    }

    func updateReadStatus(_ status: Bool) {
        isRead = status
    }

}

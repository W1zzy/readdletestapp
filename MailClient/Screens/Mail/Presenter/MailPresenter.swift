//
//  MailPresenter.swift
//  MailClient
//
//  Created by Антон Братчик on 12.07.2021.
//

import Foundation

protocol MailNavigationDelegate: AnyObject {

    func closeMail()

}

protocol MailPresenting: Presenting {

    func changeReadStatus()
    func delete()

}

class MailPresenter {

    // MARK: Injected

    private let id: Int
    private let mailSource: MailSource

    weak var navigationDelegate: MailNavigationDelegate?
    weak var view: MailInput?

    init(id: Int,
         mailSource: MailSource) {
        self.id = id
        self.mailSource = mailSource
    }

}

// MARK: - MailPresenting

extension MailPresenter: MailPresenting {

    func viewLoaded() {
        guard let mail = mailSource.mail(for: id) else {
            view?.showEmptyView()
            return
        }
        view?.updateView(with: MailViewModel(date: mail.date,
                                             from: mail.from,
                                             subject: mail.subject))
        view?.updateReadStatus(mail.isRead)

        mailSource.addListener(self)

        view?.startLoading()
        mailSource.fetchMail(by: id)
    }

    func viewUnloaded() {
        mailSource.removeListener(self)
    }

    func changeReadStatus() {
        mailSource.changeReadStatus(id)
    }

    func delete() {
        mailSource.delete(id)
    }

}

// MARK: - MailServiceDetailsListener

extension MailPresenter: MailServiceDetailsListener {

    func changeReadStatus(for id: Int, on status: Bool) {
        guard self.id == id else { return }
        view?.updateReadStatus(status)
    }

    func deleted(id: Int) {
        guard self.id == id else { return }
        view?.showEmptyView()
        navigationDelegate?.closeMail()
    }

    func received(error: String) {
        view?.showError(error)
    }

    func loaded(mail: Mail) {
        view?.stopLoading()
        guard id == mail.id else { return }
        view?.updateView(with: mail.content)
    }

}

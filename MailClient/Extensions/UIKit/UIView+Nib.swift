//
//  UIView+Nib.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import UIKit

// MARK: - Nib

extension UIView {
    static var nib: UINib {
        let bundle = Bundle(for: self as AnyClass)
        let nib = UINib(nibName: String(describing: self), bundle: bundle)
        return nib
    }

    @discardableResult
    func fromNib<T: UIView>() -> T? {
        guard let contentView = Bundle(for: type(of: self)).loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)?.first as? T else {
            return nil
        }
        self.addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.pinViewToEdgesOfSuperview()
        return contentView
    }
}

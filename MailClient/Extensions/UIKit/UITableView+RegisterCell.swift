//
//  UITableView+RegisterCell.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import UIKit

extension UITableView {

    /**
     The shorter method for reusable cell registering
     */
    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier(), for: indexPath) as? T else {
            #if DEBUG
            fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier())")
            #else
            return T()
            #endif
        }

        return cell
    }

    func dequeueHeaderFooter<T: UITableViewHeaderFooterView>(for type: T.Type) -> T {
        guard let headerFooter = dequeueReusableHeaderFooterView(withIdentifier: T.reuseIdentifier()) as? T else {
            fatalError("Wrong headerFooter type")
        }
        return headerFooter
    }

}

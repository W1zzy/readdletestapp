//
//  UIView+Nib.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import UIKit

protocol ReusableView {
    static func reuseIdentifier() -> String
}

extension ReusableView {
    static func reuseIdentifier() -> String {
        return String(describing: self)
    }
}

// MARK: - UITableViewCell

extension ReusableView where Self: UITableViewCell {
    static func registerFor(tableView: UITableView) {
        tableView.register(nib, forCellReuseIdentifier: Self.reuseIdentifier())
    }
}

extension UITableViewCell: ReusableView {}

// MARK: - UITableViewCellHeaderFooter

extension ReusableView where Self: UITableViewHeaderFooterView {
    static func registerFor(tableView: UITableView) {
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: Self.reuseIdentifier())
    }
}

extension UITableViewHeaderFooterView: ReusableView {}

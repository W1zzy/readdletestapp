//
//  UIViewController+Init.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import UIKit

extension UIViewController: DefaultIdentifier {
    func child<T: UIViewController>() -> T? {
        if let viewController = children.first(where: { $0 is T }) as? T {
            return viewController
        }
        return nil
    }

    func allChildren() -> [UIViewController] {
        return children
    }

    func removeChildViewControllerFromParent() {
        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }

    func addChildController <T: UIViewController>(with stroryboardName: StoryboardName, in containerView: UIView) -> T {
        let controller: T = T.instantiate(with: stroryboardName)
        addChildViewController(controller, into: containerView)
        return controller
    }

    func addChildViewController(_ childViewController: UIViewController, into containerView: UIView) {
        addChild(childViewController)
        childViewController.view.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(childViewController.view)
        NSLayoutConstraint.activate([
            childViewController.view.topAnchor.constraint(equalTo: containerView.topAnchor),
            childViewController.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            childViewController.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            childViewController.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor)
            ])
        childViewController.didMove(toParent: self)
    }

    static func instantiate <T: UIViewController>(with stroryboardName: StoryboardName) -> T {
        guard let controller = UIStoryboard(name: stroryboardName.rawValue, bundle:Bundle.main).instantiateViewController(withIdentifier: T.defaultIdentifier) as? T else {
            fatalError("Could not dequeue controller with identifier: \(T.defaultIdentifier) in storyboard: \(stroryboardName)")
        }
        return controller
    }
}

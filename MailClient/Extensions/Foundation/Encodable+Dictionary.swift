//
//  Encodable+Dictionary.swift
//  MailClient
//
//  Created by Антон Братчик on 17.07.2021.
//

import Foundation

extension Encodable {

    func asDictionary() throws -> [String: Any] {
        let data = try JSONEncoder().encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        return dictionary
    }

}

//
//  NSObject+Identifier.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import Foundation

protocol DefaultIdentifier: class {
    static var defaultIdentifier: String { get }
}

extension DefaultIdentifier where Self: NSObject {
    static var defaultIdentifier: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
}

extension NSObject {
    var className: String {
        get {
            return NSStringFromClass(type(of: self))
        }
    }
}

//
//  Dictionary+Merge.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import Foundation

func += <K, V> (left: inout [K: V], right: [K: V]) {
    for (k, v) in right {
        left[k] = v
    }
}

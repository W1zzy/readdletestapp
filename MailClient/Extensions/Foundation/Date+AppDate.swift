//
//  Date+AppDate.swift
//  MailClient
//
//  Created by Антон Братчик on 12.07.2021.
//

import Foundation

extension Date {

    var formattedString: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return dateFormatter.string(from: self)
    }

}

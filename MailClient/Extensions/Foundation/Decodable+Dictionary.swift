//
//  Decodable+Dictionary.swift
//  MailClient
//
//  Created by Антон Братчик on 17.07.2021.
//

import Foundation

extension Dictionary {

    func decode<T>(_ type: T.Type) throws -> T where T : Decodable {
        let data = try JSONSerialization.data(withJSONObject: self, options: [])
        return try JSONDecoder().decode(type, from: data)
    }

}

//
//  String+Constants.swift
//  MailClient
//
//  Created by Антон Братчик on 07.07.2021.
//

import Foundation

extension String {

    static let empty = ""
    static let space = " "
    
}

//
//  RequestSending.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import Foundation

enum ServiceError: LocalizedError {
    case somethingWrong

    var localizedDescription: String {
        switch self {
        case .somethingWrong:
            return "Something wrong happend"
        }
    }
}

protocol RequestSending {

    func dataTask<T: Decodable>(with request: URLRequest,
                                session: URLSession,
                                response: T.Type,
                                completion: @escaping NetworkHandler<T>)

}

extension RequestSending {

    func dataTask<T: Decodable>(with request: URLRequest,
                                session: URLSession,
                                response: T.Type,
                                completion: @escaping NetworkHandler<T>) {
        session.dataTask(with: request) { data, httpResponse, error in
            // Main handling
            func mainCompletion<A>(_ completion: @escaping NetworkHandler<A>, result: NetworkResult<A>) {
                DispatchQueue.main.async {
                    completion(result)
                }
            }

            // Check client error
            if error != nil {
                return mainCompletion(completion, result: .failure(.somethingWrong))
            }

            // Check server error
            guard let httpResponse = httpResponse as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                    return mainCompletion(completion, result: .failure(.somethingWrong))
            }

            guard let data = data else {
                return mainCompletion(completion, result: .failure(.somethingWrong))
            }

            // Get the value from the body, could be change of decoding factory, for test
            // left like it is, assume that we will work only with JSON data for now
            guard let value = try? JSONDecoder().decode(T.self, from: data) else {
                return mainCompletion(completion, result: .failure(.somethingWrong))
            }

            // Finished with result
            mainCompletion(completion, result: .success(value))
        }.resume()
    }

}

//
//  RequestEncoder.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import Foundation

protocol RequestEncoder {

    func encodeBody(_ parameters: Parameters) -> Data?
    func encodeUrlItems(_ parameters: Parameters) -> [URLQueryItem]?

}

extension RequestEncoder {

    func encodeBody(_ parameters: Parameters) -> Data? { return nil }
    func encodeUrlItems(_ parameters: Parameters) -> [URLQueryItem]? { return nil }

}

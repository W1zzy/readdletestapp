//
//  ParametersJSONEncoder.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import Foundation

class ParametersJSONEncoder: RequestEncoder {

    func encodeBody(_ parameters: Parameters) -> Data? {
        return try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
    }

}

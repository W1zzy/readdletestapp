//
//  HttpParametersEncoderFactory.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import Foundation

protocol ParametersEncoderFactory {

    associatedtype Encoder

    static func parametersEncoder(forType codingType: ParameterEncoding) -> Encoder

}

class HttpParametersEncoderFactory: ParametersEncoderFactory {

    static func parametersEncoder(forType codingType: ParameterEncoding) -> RequestEncoder {
        switch codingType {
        case .json:
            return ParametersJSONEncoder()
        case .urlEncoded:
            return ParametersURLEncoder()
        }
    }

}

//
//  Networking.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import Foundation

typealias NetworkResult<T> = Result<T, ServiceError>
typealias NetworkHandler<T> = (NetworkResult<T>) -> Void

protocol Networking {

    func sendRequest<T: Decodable, R: Endpoint>(_ request: R,
                                                response: T.Type,
                                                completion: @escaping NetworkHandler<T>)

}

class NetworkService: Networking, RequestSending {

    private let urlSession: URLSession

    // MARK: Initialization

    init(urlSession: URLSession = URLSession.shared) {
        self.urlSession = urlSession
    }

    // MARK: Networking

    func sendRequest<T: Decodable, R: Endpoint>(_ request: R,
                                                response: T.Type,
                                                completion: @escaping NetworkHandler<T>) {
        guard let urlRequest = request.makeURLRequest() else {
            completion(.failure(.somethingWrong))
            return
        }

        dataTask(with: urlRequest,
                 session: urlSession,
                 response: response,
                 completion: completion)
    }

}

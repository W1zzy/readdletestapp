//
//  ParameterEncoding.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import Foundation

enum ParameterEncoding: String {

    case json
    case urlEncoded

}

extension ParameterEncoding {

    var httpHeader: String {
        switch self {
        case .json :
            return "application/json"
        case .urlEncoded :
            return "application/x-www-form-urlencoded"
        }
    }

    var codingHeaders: HTTPHeaders {
        return ["Content-Type": httpHeader]
    }

}

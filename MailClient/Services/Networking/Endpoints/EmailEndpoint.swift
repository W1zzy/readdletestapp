//
//  EmailEndpoint.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import Foundation

enum EmailEndpoint {
    case getEmails(page: Int)
    case getEmail(id: Int)
}

extension EmailEndpoint: Endpoint {

    var baseURL: String {
        switch self {
        case .getEmails:
            return "https://my-json-server.typicode.com/w1zzy/db/"
        case .getEmail:
            return "https://my-json-server.typicode.com/Anton99998/db/"
        }
    }

    var path: String {
        switch self {
        case .getEmails(let page):
            return "emails/\(page)"
        case .getEmail(let id):
            return "emails/\(id)"
        }
    }

    var httpMethod: HTTPMethod {
        switch self {
        default:
            return .get
        }
    }

    var parameters: Parameters? {
        switch self {
        default:
            return nil
        }
    }

    var encoding: ParameterEncoding {
        switch self {
        default:
            return .urlEncoded
        }
    }
}

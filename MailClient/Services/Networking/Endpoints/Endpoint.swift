//
//  Endpoint.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import Foundation

typealias HTTPHeaders = [String: String]
typealias Parameters = [String: Any]

protocol Endpoint {
    var baseURL: String { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var headers: HTTPHeaders? { get }
    var url: URL? { get }
    var encoding: ParameterEncoding { get }
    var parameters: Parameters? { get }

    func makeURLRequest() -> URLRequest?
}

extension Endpoint {

    var url: URL? {
        switch self {
        default:
            return URL(string: self.baseURL + self.path)
        }
    }

    var encoding: ParameterEncoding {
        switch self {
        default:
            return .json
        }
    }

    var headers: HTTPHeaders? {
        switch self {
        default:
            return ["Content-Type": "application/json"]
        }
    }

    func makeURLRequest() -> URLRequest? {
        guard let url = self.url else {
            return nil
        }

        var components = URLComponents()

        components.scheme = url.scheme
        components.host = url.host
        components.port = url.port
        components.path = url.path
        components.queryItems = queryItems

        var urlRequest = URLRequest(url: url)

        urlRequest.httpMethod = httpMethod.rawValue

        var allHeaders = headers
        allHeaders? += encoding.codingHeaders

        urlRequest.allHTTPHeaderFields = allHeaders
        urlRequest.httpBody = body

        return urlRequest
    }

}

private extension Endpoint {

    var body: Data? {
        guard let parameters = parameters else { return nil }
        return HttpParametersEncoderFactory.parametersEncoder(forType: encoding).encodeBody(parameters)
    }

    var queryItems: [URLQueryItem]? {
        guard let parameters = parameters else { return nil }
        return HttpParametersEncoderFactory.parametersEncoder(forType: encoding).encodeUrlItems(parameters)
    }

}

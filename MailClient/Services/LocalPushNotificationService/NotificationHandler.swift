//
//  NotificationHandler.swift
//  MailClient
//
//  Created by Антон Братчик on 17.07.2021.
//

import Foundation
import UserNotifications

protocol NotificationHandling {

    func openScreenUsingUserInfo(_ userInfo: [AnyHashable : Any])

}

class NotificationHandler: NotificationHandling {

    private let mailService: MailSource?
    private let coordinator: MailCoordinator?

    init(with coordinator: MailCoordinator?,
         mailService: MailSource?) {
        self.coordinator = coordinator
        self.mailService = mailService
    }

    func openScreenUsingUserInfo(_ userInfo: [AnyHashable : Any]) {
        guard let mail = try? userInfo.decode(Mail.self) else { return }
        coordinator?.openMail(mail)
        mailService?.changeReadStatus(mail.id)
    }

}

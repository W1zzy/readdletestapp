//
//  NotificationService.swift
//  MailClient
//
//  Created by Антон Братчик on 15.07.2021.
//

import Foundation
import UserNotifications

// MARK: - NotificationType

enum NotificationType {

    case mail(_ userInfo: [String: Any])

    var title: String {
        switch self {
        case .mail:
            return "New e-mail!"
        }
    }

    var desc: String {
        switch self {
        case .mail:
            return "Something interesting here"
        }
    }

    var time: TimeInterval {
        switch self {
        case .mail:
            return .zero
        }
    }

    var shouldRepeat: Bool {
        switch self {
        case .mail:
            return false
        }
    }

    var identifier: String {
        switch self {
        case .mail:
            return "NEW_MAIL"
        }
    }

}

protocol NotificationServicing: AnyObject {

    var delegate: NotificationServiceDelegate? { get set }

    func checkAuthorization()
    func addNotification(_ type: NotificationType)

}

protocol NotificationServiceDelegate: class {

    func noPermissionsGranted()

}

class NotificationService: NotificationServicing {

    // MARK: Injected

    weak var delegate: NotificationServiceDelegate?

    // MARK: Properties

    private let notificationCenter = UNUserNotificationCenter.current()
    private let options: UNAuthorizationOptions = [.alert, .sound, .badge]
    private let identifier = "MailIdentifier"

    // MARK: Methods

    func checkAuthorization() {
        notificationCenter.getNotificationSettings { [weak self] (settings) in
            DispatchQueue.main.async {
                switch settings.authorizationStatus {
                case .notDetermined:
                    self?.requestAuthorization()
                case .denied:
                    self?.delegate?.noPermissionsGranted()
                default:
                    break
                }
            }
        }
    }

    func addNotification(_ type: NotificationType) {
        let trigger: UNTimeIntervalNotificationTrigger?

        if type.time == .zero {
            trigger = nil
        } else {
            trigger = UNTimeIntervalNotificationTrigger(timeInterval: type.time,
                                                        repeats: type.shouldRepeat)
        }
        let content = UNMutableNotificationContent()
        content.title = type.title
        content.body = type.desc
        content.sound = UNNotificationSound.default

        if case .mail(let userInfo) = type {
            content.userInfo = userInfo
        }
        let request = UNNotificationRequest(identifier: type.identifier,
                                            content: content,
                                            trigger: trigger)

        notificationCenter.add(request)
    }

}

// MARK: - Helpers

private extension NotificationService {

    private func requestAuthorization() {
        notificationCenter.requestAuthorization(options: options) {
            [weak self] allowed, error in
            DispatchQueue.main.async {
                if !allowed {
                    self?.delegate?.noPermissionsGranted()
                }
            }
        }
    }

}

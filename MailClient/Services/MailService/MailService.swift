//
//  MailService.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import Foundation

protocol Listener: AnyObject {

    func received(error: String)

}

protocol MailServiceListener: Listener {

    func update()
    func appendOneAtTop()
    func append(count: Int)
    func deleted(at index: Int)
    func reload(at index: Int)
    func lastPageReceived()

}

protocol MailServiceDetailsListener: Listener {

    func loaded(mail: Mail)

    func deleted(id: Int)
    func changeReadStatus(for id: Int, on status: Bool)

}

protocol MailServicing: AnyObject {

    func addListener(_ listener: Listener)
    func removeListener(_ listener: Listener)

}

protocol MailSource: MailServicing {

    func fetchMail(by id: Int)

    func mail(for id: Int) -> Mail?

    func changeReadStatus(_ id: Int)
    func delete(_ id: Int)

}

protocol MailListSource: MailServicing {

    func fetchMailList()

    func mailsCount() -> Int
    func itemFor(index: Int) -> Mail
    func itemSelectedFor(index: Int) -> Mail

    func changeReadStatus(at index: Int)
    func delete(at index: Int)

}

class MailService: MailServicing {

    // MARK: Injected

    private let networking: Networking
    private var listeners = [MailServiceListener]()
    private var detailsListeners = [MailServiceDetailsListener]()

    // MARK: Properties

    private var dataSource = SynchronizedArray<Mail>()
    private var existingMails: SynchronizedArray<Mail> { dataSource.filter { !$0.isDeleted } }

    private var page = Page.initial
    private var isLastPage = false

    // MARK: Lifecycle

    init(with networking: Networking) {
        self.networking = networking
    }

    // MARK: Listeners

    func addListener(_ listener: Listener) {
        if let listener = listener as? MailServiceDetailsListener {
            detailsListeners.append(listener)
        } else if let listener = listener as? MailServiceListener {
            listeners.append(listener)
        }
    }

    func removeListener(_ listener: Listener) {
        if let listener = listener as? MailServiceDetailsListener {
            detailsListeners.removeAll(where: {
                $0 === listener
            })
        } else if let listener = listener as? MailServiceListener {
            listeners.removeAll(where: {
                $0 === listener
            })
        }
    }

    func changeReadStatus(_ id: Int) {
        changeReadStatus(id: id)
    }

    private func changeReadStatus(id: Int, on status: Bool? = nil) {
        if let existingIndex = existingMails.firstIndex(where: { $0.id == id }),
           let index = dataSource.firstIndex(where: { $0.id == id }) {
            if let status = status {
                dataSource[index].isRead = status
            } else {
                dataSource[index].isRead.toggle()
            }

            listeners.forEach {
                $0.reload(at: existingIndex)
            }

            detailsListeners.forEach {
                $0.changeReadStatus(for: id, on: dataSource[index].isRead)
            }
        }
    }

    func delete(_ id: Int) {
        if let existingIndex = existingMails.firstIndex(where: { $0.id == id }),
           let index = dataSource.firstIndex(where: { $0.id == id }) {
            dataSource[index].isDeleted.toggle()

            listeners.forEach {
                $0.deleted(at: existingIndex)
            }

            detailsListeners.forEach {
                $0.deleted(id: id)
            }
        }
    }

}

// MARK: - MailSource

extension MailService: MailSource {

    func mail(for id: Int) -> Mail? {
        if let index = dataSource.firstIndex(where: { $0.id == id }) {
            return dataSource[index]
        }
        return nil
    }

    func fetchMail(by id: Int) {
        guard let index = dataSource.firstIndex(where: { $0.id == id }) else { return }

        if dataSource[index].isLoaded {
            self.detailsListeners.forEach {
                $0.loaded(mail: self.dataSource[index])
            }
            return
        }

        networking.sendRequest(EmailEndpoint.getEmail(id: id),
                               response: MailResponse.self) { [weak self] response in
            guard let self = self else { return }
            switch response {
            case .success(let mail):
                self.dataSource[index].set(content: mail.content)
                self.detailsListeners.forEach {
                    $0.loaded(mail: self.dataSource[index])
                }
            case .failure(let error):
                self.detailsListeners.forEach {
                    $0.received(error: error.localizedDescription)
                }
            }
        }
    }
    
}

// MARK: - MailListSource

extension MailService: MailListSource {

    func mailsCount() -> Int {
        existingMails.count
    }

    func itemFor(index: Int) -> Mail {
        existingMails[index]
    }

    func itemSelectedFor(index: Int) -> Mail {
        let selected = existingMails[index]

        changeReadStatus(id: selected.id, on: true)

        return selected
    }

    func changeReadStatus(at index: Int) {
        guard existingMails.count > index else { return }
        changeReadStatus(existingMails[index].id)
    }

    func delete(at index: Int) {
        guard existingMails.count > index else { return }
        delete(existingMails[index].id)
    }

    func fetchMailList() {
        // Assume that if we're getting error then there won't be any content, reload not implemented
        guard !page.isLast else { return }

        networking.sendRequest(EmailEndpoint.getEmails(page: page.number),
                               response: MailPageResponse.self) { [weak self] response in
            guard let self = self else { return }
            switch response {
            case .success(let page):
                self.dataSource.append(
                    page.mail.map {
                        Mail(with: $0)
                    }.sorted {
                        $0.date.compare($1.date) == .orderedDescending
                    }
                )

                if page.mail.count < self.page.limit {
                    self.page.isLast = true
                    self.listeners.forEach {
                        $0.lastPageReceived()
                    }
                }

                self.listeners.forEach {
                    if self.page.isFirst == true {
                        $0.update()
                    } else {
                        $0.append(count: page.mail.count)
                    }
                }
                self.page.increment()
            case .failure(let error):
                self.listeners.forEach {
                    $0.received(error: error.localizedDescription)
                }
                // I'm assuming that we won't have reload, once error = always error
                self.page.isLast = true
                self.listeners.forEach {
                    $0.lastPageReceived()
                }
            }
        }
    }

}

// MARK: - FakeMailReceiver

extension MailService: FakeMailReceiver {

    func newMailReceived(_ mail: Mail) {
        dataSource.insert(mail, at: .zero)
        listeners.forEach {
            $0.appendOneAtTop()
        }
    }

}

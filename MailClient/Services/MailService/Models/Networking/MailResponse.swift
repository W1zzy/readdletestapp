//
//  MailResponse.swift
//  MailClient
//
//  Created by Антон Братчик on 07.07.2021.
//

import Foundation

struct MailResponse: Codable {
    let id: Int
    let content: String
}

//
//  MailPageResponse.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import Foundation

struct MailPageResponse: Codable {
    let id: Int
    let mail: [MailListItemResponse]
}

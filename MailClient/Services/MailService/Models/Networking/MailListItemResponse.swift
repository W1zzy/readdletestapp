//
//  MailListItemResponse.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import Foundation

struct MailListItemResponse {
    let id: Int
    let date: Date
    let from: String
    let subject: String
    let preview: String
    var isRead: Bool
    var isDeleted: Bool
}

extension MailListItemResponse: Codable {

    enum CodingKeys: String, CodingKey {
        case id, date, from, subject, preview, isRead, isDeleted
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        id = try container.decode(Int.self, forKey: .id)
        from = try container.decode(String.self, forKey: .from)
        subject = try container.decode(String.self, forKey: .subject)
        preview = try container.decode(String.self, forKey: .preview)
        isRead = try container.decode(Bool.self, forKey: .isRead)
        isDeleted = try container.decode(Bool.self, forKey: .isDeleted)

        let timestamp = try container.decode(TimeInterval.self, forKey: .date)
        date = Date(timeIntervalSince1970: timestamp)
    }

}

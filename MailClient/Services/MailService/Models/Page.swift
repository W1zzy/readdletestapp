//
//  Page.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import Foundation

struct Page {
    var number: Int
    var limit: Int
    var isLast: Bool

    var isFirst: Bool {
        number == 1
    }

    mutating func increment() {
        number += 1
    }

    static var initial: Page {
        Page(number: 1,
             limit: 10,
             isLast: false)
    }
}

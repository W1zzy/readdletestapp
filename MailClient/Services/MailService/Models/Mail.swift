//
//  Mail.swift
//  MailClient
//
//  Created by Антон Братчик on 07.07.2021.
//

import Foundation

struct Mail: Codable {
    let id: Int
    let date: Date
    var content: String?
    let from: String
    let subject: String
    let preview: String
    var isRead: Bool
    var isDeleted: Bool

    var isLoaded: Bool {
        content != nil
    }

    mutating func set(content: String) {
        self.content = content
    }

    init(id: Int,
         date: Date,
         content: String?,
         from: String,
         subject: String,
         preview: String,
         isRead: Bool,
         isDeleted: Bool) {
        self.id = id
        self.date = date
        self.content = content
        self.from = from
        self.subject = subject
        self.preview = preview
        self.isRead = isRead
        self.isDeleted = isDeleted
    }

    init(with response: MailListItemResponse) {
        id = response.id
        date = response.date
        from = response.from
        subject = response.subject
        preview = response.preview
        isRead = response.isRead
        isDeleted = response.isDeleted
    }
}

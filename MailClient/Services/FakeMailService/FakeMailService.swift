//
//  FakeMailService.swift
//  MailClient
//
//  Created by Антон Братчик on 15.07.2021.
//

import UIKit

protocol FakeMailReceiver: AnyObject {

    func newMailReceived(_ mail: Mail)

}

class FakeMailService {

    // MARK: Injected

    weak var receiver: FakeMailReceiver?
    weak var notificationService: NotificationServicing?

    // MARK: Properties

    private var backgroundTaskID = UIBackgroundTaskIdentifier.invalid
    private var timer: Timer?

    // MARK: Methods

    func startFaking() {
        // Repeated local push notifications need 60 seconds time interval, this
        // keeps app alive and gives us opportunity to send several notifications without
        // time interval
        self.backgroundTaskID = UIApplication.shared.beginBackgroundTask (withName: "Finish Network Tasks")

        timer = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) { [weak self] _ in
            let newMail = Mail.mockMail
            guard let dict = try? newMail.asDictionary() else { return }
            self?.notificationService?.addNotification(.mail(dict))
            self?.receiver?.newMailReceived(newMail)
        }
    }

}

private extension Mail {

    static var mockMail: Mail {
        Mail(id: Int.random(in: 13..<100000000),
             date: Date(),
             content: "Test content",
             from: "Test",
             subject: "Testing",
             preview: "Test content",
             isRead: false,
             isDeleted: false)
    }

}

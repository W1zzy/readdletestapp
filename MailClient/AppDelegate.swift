//
//  AppDelegate.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import UIKit
import UserNotifications

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    // MARK: Services

    private var mailCoordinator: MailCoordinator?
    private var notificationHandler: NotificationHandling?
    private let mailService = MailService(with: NetworkService())
    private let notificationService = NotificationService()
    private let fakeMailService = FakeMailService()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        prepareApplication()
        startFakeService()
        startNotificationHandler()
        
        return true
    }

}

// MARK: - Helpers

private extension AppDelegate {

    func prepareApplication() {
        UNUserNotificationCenter.current().delegate = self

        let splitViewController =  UISplitViewController()

        mailCoordinator = MailCoordinator(splitView: splitViewController)
        mailCoordinator?.mailService = mailService
        mailCoordinator?.notificationService = notificationService

        mailCoordinator?.start()

        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = splitViewController
        self.window?.makeKeyAndVisible()
    }

    func startFakeService() {
        fakeMailService.receiver = mailService
        fakeMailService.notificationService = notificationService
        fakeMailService.startFaking()
    }

    func startNotificationHandler() {
        notificationHandler = NotificationHandler(with: mailCoordinator,
                                                  mailService: mailService)
    }

}

// MARK: - UISplitViewControllerDelegate

extension AppDelegate: UISplitViewControllerDelegate {

    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        return true
    }

}

// MARK: - UNUserNotificationCenterDelegate

extension AppDelegate: UNUserNotificationCenterDelegate {

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        guard response.actionIdentifier == UNNotificationDefaultActionIdentifier else { return }
        notificationHandler?.openScreenUsingUserInfo(response.notification.request.content.userInfo)
        completionHandler()
    }

}

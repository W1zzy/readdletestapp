//
//  DataLoading.swift
//  MailClient
//
//  Created by Антон Братчик on 15.07.2021.
//

import UIKit

protocol DataLoading {

    func startLoading()
    func stopLoading()

}

extension DataLoading where Self: ActivityIndicatorHolding {

    func startLoading() {
        activityIndicator.startAnimating()
    }

    func stopLoading() {
        activityIndicator.stopAnimating()
    }

}

protocol ActivityIndicatorHolding: DataLoading {

    // IBOutlet
    var activityIndicator: UIActivityIndicatorView! { get set }

}

//
//  AlertShowing.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import UIKit

protocol AlertShowing {

    func showError(_ error: String)

}

extension AlertShowing where Self: UIViewController {

    func showError(_ error: String) {
        let alert = UIAlertController(title: "Error",
                                      message: error,
                                      preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok",
                                     style: .default)

        alert.addAction(okAction)
        present(alert, animated: true)
    }

}

//
//  PaginatableView.swift
//  MailClient
//
//  Created by Антон Братчик on 15.07.2021.
//

import UIKit

protocol PaginatableView where Self: UIScrollViewDelegate {

    var infinityScrollBlocked: Bool { get set }

    func scrollViewDidScrollOverThreshold()

}

extension PaginatableView {

    func scrollViewScrolled(_ scrollView: UIScrollView) {
        guard !infinityScrollBlocked else { return }
        let contentSize = scrollView.contentSize.height
        if contentSize > 0 {
            let scrollViewSize = scrollView.bounds.size.height
            let currentOffsetPosition = scrollView.contentOffset.y

            // 30% threshold
            if (contentSize - currentOffsetPosition < 0.3 + scrollViewSize) {
                scrollViewDidScrollOverThreshold()
            }
        }
    }

}

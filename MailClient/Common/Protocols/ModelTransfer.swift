//
//  ModelTransfer.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import Foundation

protocol ModelTransfer {
    /// Type of model that is being transferred
    associatedtype ModelType

    /// Updates view with `model`.
    func update(with model: ModelType)

    /// Appends items to excisting datasource, simplify logic (Optional)
    func appendItems(with model: ModelType)
}

extension ModelTransfer {
    func appendItems(with model: ModelType) {

    }
}

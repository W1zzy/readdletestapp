//
//  Presenting.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import Foundation

/// Initial optional methods for presenters
protocol Presenting: AnyObject {

    func viewLoaded()
    func viewUnloaded()

}

extension Presenting {

    func viewLoaded() {}
    func viewUnloaded() {}

}

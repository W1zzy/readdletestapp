//
//  SynchronizedArray.swift
//  MailClient
//
//  Created by Антон Братчик on 14.07.2021.
//

import Foundation

public class SynchronizedArray<Element> {

    private let queue = DispatchQueue(label: "SynchronizedArray", attributes: .concurrent)
    private var array = [Element]()

    init() {
        self.array = [Element]()
    }

    init(_ array: [Element]) {
        self.array = array
    }

}

extension SynchronizedArray {

    var first: Element? {
        queue.sync {
            return self.array.first
        }
    }

    var last: Element? {
        queue.sync {
            return self.array.last
        }
    }

    var count: Int {
        queue.sync {
            return self.array.count
        }
    }

    func forEach(_ body: (Element) -> Void) {
        queue.sync {
            self.array.forEach(body)
        }
    }

    func firstIndex(where predicate: (Element) -> Bool) -> Int? {
        queue.sync {
            return self.array.firstIndex(where: predicate)
        }
    }

    func filter(isIncluded: (Element) -> Bool) -> SynchronizedArray<Element> {
        queue.sync {
            SynchronizedArray(self.array.filter(isIncluded))
        }
    }

    func append( _ element: Element) {
        queue.async(flags: .barrier) {
            self.array.append(element)
        }
    }

    func append( _ elements: [Element]) {
        queue.async(flags: .barrier) {
            self.array += elements
        }
    }

    func insert( _ element: Element, at index: Int) {
        queue.async(flags: .barrier) {
            self.array.insert(element, at: index)
        }
    }

    func remove(where predicate: @escaping (Element) -> Bool, completion: ((Element) -> Void)? = nil) {
        queue.async(flags: .barrier) {
            guard let index = self.array.firstIndex(where: predicate) else { return }
            let element = self.array.remove(at: index)

            DispatchQueue.main.async {
                completion?(element)
            }
        }
    }

    func removeAll(completion: (([Element]) -> Void)? = nil) {
        queue.async(flags: .barrier) {
            let elements = self.array
            self.array.removeAll()

            DispatchQueue.main.async {
                completion?(elements)
            }
        }
    }

}

public extension SynchronizedArray {

    subscript(index: Int) -> Element {
        get {
            queue.sync {
                return self.array[index]
            }
        }
        set {
            queue.async(flags: .barrier) {
                self.array[index] = newValue
            }
        }
    }

}

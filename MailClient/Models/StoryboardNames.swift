//
//  StoryboardNames.swift
//  MailClient
//
//  Created by Антон Братчик on 06.07.2021.
//

import Foundation

enum StoryboardName: String {

    case mailList = "MailList"
    case mail = "Mail"
    
}
